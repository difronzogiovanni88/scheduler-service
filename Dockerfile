FROM openjdk:8-jre-alpine
EXPOSE 8120
ADD /target/scheduler-service-1.0.jar scheduler-service.jar
ENTRYPOINT ["java", "-jar", "scheduler-service.jar"]