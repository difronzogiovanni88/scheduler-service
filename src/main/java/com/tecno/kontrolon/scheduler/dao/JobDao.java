package com.tecno.kontrolon.scheduler.dao;

import com.tecno.kontrolon.scheduler.entity.Job;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;

@Transactional
public interface JobDao extends CrudRepository<Job, Long> {


    public Job findByNameAndType(String name, String type);
}
