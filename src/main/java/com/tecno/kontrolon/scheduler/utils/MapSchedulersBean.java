package com.tecno.kontrolon.scheduler.utils;

import com.tecno.kontrolon.scheduler.service.EventThread;
import org.springframework.stereotype.Component;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ScheduledExecutorService;

@Component
public class MapSchedulersBean {

    private String key;

    private ConcurrentHashMap threads = new ConcurrentHashMap<String, EventThread>();

    private ConcurrentHashMap taskSchedulers = new ConcurrentHashMap<String, ScheduledExecutorService>();

    public ConcurrentHashMap getThreads() {
        return threads;
    }

    public void setThreads(ConcurrentHashMap threads) {
        this.threads = threads;
    }

    public ConcurrentHashMap getTaskSchedulers() {
        return taskSchedulers;
    }

    public void setTaskSchedulers(ConcurrentHashMap taskSchedulers) {
        this.taskSchedulers = taskSchedulers;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
