package com.tecno.kontrolon.scheduler.utils;

import org.springframework.validation.ObjectError;

import java.util.List;
import java.util.stream.Collectors;

public class GenericResponse {
    private String message;
    private String error;
    private String notes;
    

    public GenericResponse(){
    	
    }
   
    public GenericResponse(final String message) {
        super();
        this.message = message;
    }

    public GenericResponse(final String message, final String error) {
        super();
        this.message = message;
        this.error = error;
    }

    public GenericResponse(List<ObjectError> allErrors, String error) {
        this.error = error;
        this.message = allErrors.stream()
            .map(e -> e.getDefaultMessage())
            .collect(Collectors.joining(","));
    }

    public String getMessage() {
        return message;
    }

    public GenericResponse setMessage(final String message) {
        this.message = message;
        return this;
    }

    public String getError() {
        return error;
    }

    public GenericResponse setError(final String error) {
        this.error = error;
        return this;
    }
    
    public GenericResponse setNotes(String notes) {
		this.notes = notes;
		return this;
	}
    
    public String getNotes() {
		return notes;
	}
    
    public boolean checkIsSucess(){
    	return this.message.equals("success");
    }

}
