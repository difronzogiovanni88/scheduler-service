package com.tecno.kontrolon.scheduler.kafka.producer;


import com.tecno.kontrolon.scheduler.kafka.properties.KafkaProperties;
import com.tecno.kontrolon.schema.json.JobScheduled;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;
import org.springframework.kafka.support.serializer.JsonSerializer;

import java.util.HashMap;
import java.util.Map;

@Configuration
public class ProducerConfig {


    @Bean
    public ProducerFactory<String, JobScheduled> producerFactory() {
        Map<String, Object> configProps = new HashMap<>();
        Object bootstrapAddress;
        configProps.put(
                org.apache.kafka.clients.producer.ProducerConfig.BOOTSTRAP_SERVERS_CONFIG,
                KafkaProperties.BOOTSTRAP);
        configProps.put(
                org.apache.kafka.clients.producer.ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG,
                StringSerializer.class);
        configProps.put(
                org.apache.kafka.clients.producer.ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG,
                JsonSerializer.class);
        return new DefaultKafkaProducerFactory<>(configProps);
    }

    @Bean
    @Scope("prototype")
    public KafkaTemplate<String, JobScheduled> kafkaTemplate() {
        return new KafkaTemplate<>(producerFactory());
    }

}
