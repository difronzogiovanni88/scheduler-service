package com.tecno.kontrolon.scheduler.kafka.properties;

public class KafkaProperties {


    public static   String BOOTSTRAP = "localhost:9092";
    public static  String METRIC_TOPIC = "metric_scheduled";
    public static String ALERT_TOPIC = "alert_scheduled";
}
