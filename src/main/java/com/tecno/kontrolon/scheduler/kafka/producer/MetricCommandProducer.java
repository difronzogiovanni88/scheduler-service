package com.tecno.kontrolon.scheduler.kafka.producer;

import com.tecno.kontrolon.scheduler.kafka.properties.KafkaProperties;
import com.tecno.kontrolon.schema.json.JobScheduled;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
public class MetricCommandProducer {

    @Autowired
    KafkaTemplate<String, JobScheduled> kafkaTemplate;

    public void sendMessage(JobScheduled jobScheduled){
        kafkaTemplate.send(KafkaProperties.METRIC_TOPIC, jobScheduled);
    }

}
