package com.tecno.kontrolon.scheduler.service.impl;

import com.tecno.kontrolon.scheduler.dto.JobDto;
import com.tecno.kontrolon.scheduler.service.EventThread;
import com.tecno.kontrolon.scheduler.service.ScheduleProducerService;
import com.tecno.kontrolon.scheduler.utils.MapSchedulersBean;
import com.tecno.kontrolon.schema.json.JobScheduled;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Service
public class ScheduleProducerServiceImpl implements ScheduleProducerService {

    @Autowired
    private MapSchedulersBean mapSchedulersBean;

    @Autowired
    private KafkaTemplate<String, JobScheduled> kafkaTemplate;

    // @Autowired
    //private EventThread eventThread;

//    @Autowired
//    private
//    ScheduledExecutorService scheduledExecutorService;


    public String produce(JobDto jobDto) {
        try {
            ConcurrentHashMap threads = mapSchedulersBean.getThreads();
            ConcurrentHashMap executors = mapSchedulersBean.getTaskSchedulers();

            String key = jobDto.getType().toLowerCase() + "_" + jobDto.getName() + "_job";

            ApplicationContext ctx = new ApplicationContext();
            EventThread eventThread = ctx.getEventThreadBean();
            //  EventThread  eventThread = (EventThread)ctx.getBean("eventThread") ;

            //    ScheduledExecutorService scheduledExecutorService = (ScheduledExecutorService)ctx.getBean("scheduledExecutorServicer");
            ScheduledExecutorService scheduledExecutorService = Executors.newScheduledThreadPool(1);


            if (threads.containsKey(key)) {
                EventThread previousThread = (EventThread) threads.get(key);
                ScheduledExecutorService previousExecutor = (ScheduledExecutorService) executors.get(key);
                previousExecutor.shutdown();
                previousThread.interrupt();
                threads.remove(key);
                executors.remove(key);
            }
            eventThread.setJobDto(jobDto);
            threads.put(key, eventThread);
            executors.put(key, scheduledExecutorService);


            Set<Map.Entry<String, EventThread>> entrySet = threads.entrySet();
            Iterator<Map.Entry<String, EventThread>> itr = entrySet.iterator();


            while (itr.hasNext()) {
                Map.Entry<String, EventThread> entry = itr.next();
                EventThread thread = entry.getValue();
                String k = entry.getKey();
                if (!thread.isInterrupted()) {
                    ScheduledExecutorService ex = (ScheduledExecutorService) executors.get(k);
                    ex.scheduleAtFixedRate(thread, 0, Long.parseLong(thread.getJobDto().getScheduleTime()), TimeUnit.MILLISECONDS);
                    //  ScheduledExecutorService executor = scheduledExecutorService;
                    //   executor.scheduleAtFixedRate(eventThread, 0, Long.parseLong(jobDto.getScheduleTime()), TimeUnit.MILLISECONDS);


                    //        ConcurrentTaskScheduler cuncurreConcurrentTaskScheduler = new ConcurrentTaskScheduler();
                    //        cuncurreConcurrentTaskScheduler.scheduleAtFixedRate(eventThread, Long.parseLong(jobDto.getScheduleTime()) );

                }
            }
        }catch(Exception ex){
            return "error";
        }


        return "success";
    }

    @Override
    public String delete(String jobName) {
        try {
            String key = jobName + "_job";
            ConcurrentHashMap threads = mapSchedulersBean.getThreads();
            ConcurrentHashMap executors = mapSchedulersBean.getTaskSchedulers();
            if (threads.containsKey(key)) {
                EventThread toInterrupt = (EventThread) mapSchedulersBean.getThreads().get(key);
                ScheduledExecutorService ToShutDown = (ScheduledExecutorService) mapSchedulersBean.getTaskSchedulers().get(key);
                ToShutDown.shutdown();
                toInterrupt.interrupt();
                threads.remove(key);
                executors.remove(key);

                Set<Map.Entry<String, EventThread>> entrySet = threads.entrySet();
                Iterator<Map.Entry<String, EventThread>> itr = entrySet.iterator();


                while (itr.hasNext()) {
                    Map.Entry<String, EventThread> entry = itr.next();
                    EventThread thread = entry.getValue();
                    String k = entry.getKey();
                    if (!thread.isInterrupted()) {
                        ScheduledExecutorService ex = (ScheduledExecutorService) executors.get(k);
                        ex.scheduleAtFixedRate(thread, 0, Long.parseLong(thread.getJobDto().getScheduleTime()), TimeUnit.MILLISECONDS);

                    }
                }
                return "success";
            } else
                return "error";

        } catch (Exception ex) {
            return "error";
        }
    }
}
