package com.tecno.kontrolon.scheduler.service.impl;

import com.tecno.kontrolon.scheduler.AppConfig;
import com.tecno.kontrolon.scheduler.service.EventThread;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class ApplicationContext implements ApplicationContextAware {

    ApplicationContext context;

    @Override
    public void setApplicationContext(org.springframework.context.ApplicationContext applicationContext) throws BeansException {

        this.context=context;
    }

    public EventThread  getEventThreadBean(){
        AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext(AppConfig.class);
        EventThread eventThread =(EventThread)ctx.getBean(EventThread.class);
        return eventThread;
    }
}
