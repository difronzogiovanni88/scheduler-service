package com.tecno.kontrolon.scheduler.service;

import com.tecno.kontrolon.scheduler.dto.JobDto;


public interface ScheduleProducerService {
    public String produce(JobDto jobDto);
    public String delete(String jobName);
}
