package com.tecno.kontrolon.scheduler.service;

import com.tecno.kontrolon.scheduler.dto.JobDto;
import com.tecno.kontrolon.scheduler.kafka.producer.AlertCommandProducer;
import com.tecno.kontrolon.scheduler.kafka.producer.MetricCommandProducer;
import com.tecno.kontrolon.scheduler.utils.Constants;
import com.tecno.kontrolon.schema.json.JobScheduled;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component("eventThread")
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class EventThread extends Thread implements Runnable {

    public JobDto jobDto;
    @Autowired
    MetricCommandProducer metricCommandProducer ;

    @Autowired
    AlertCommandProducer alertCommandProducer ;

    @Autowired
    KafkaTemplate<String, JobScheduled> kafkaTemplate;




    public EventThread() {

    }

    public JobDto getJobDto() {
        return jobDto;
    }

    public void setJobDto(JobDto jobDto) {
        this.jobDto = jobDto;
    }



    public void run(){
        JobScheduled jobScheduled = new JobScheduled();
        jobScheduled.setName(jobDto.getName());
        jobScheduled.setTimestamp(new Date().getTime());

        if(jobDto.isActive() == true) {
            if (jobDto.getType().equals(Constants.METRICA_TYPE)) {
               metricCommandProducer.sendMessage(jobScheduled);
//                System.out.println(metricScheduled.toString());

            }
            if (jobDto.getType().equals(Constants.ALERT_TYPE)) {

                alertCommandProducer.sendMessage(jobScheduled);
            }
        }else
            this.interrupt();

    }


}
