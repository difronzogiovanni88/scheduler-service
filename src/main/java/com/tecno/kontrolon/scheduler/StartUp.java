package com.tecno.kontrolon.scheduler;

import com.tecno.kontrolon.scheduler.dao.JobDao;
import com.tecno.kontrolon.scheduler.dto.JobDto;
import com.tecno.kontrolon.scheduler.entity.Job;
import com.tecno.kontrolon.scheduler.service.ScheduleProducerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationStartedEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Component
public class StartUp implements ApplicationListener<ApplicationStartedEvent> {

    @Autowired
    private JobDao jobDao;

    @Autowired
    private ScheduleProducerService scheduleProducerService;

    @Override
    public void onApplicationEvent(ApplicationStartedEvent applicationStartedEvent) {
        List<Job> storedJobs = new ArrayList<>();
        Iterator<Job> itr = jobDao.findAll().iterator();
        while(itr.hasNext()){
            Job job = itr.next();
            JobDto jobDto = new JobDto();
            jobDto.setName(job.getName());
            jobDto.setActive(job.isActive());
            jobDto.setScheduleTime(job.getScheduleTime());
            jobDto.setType(job.getType());
            scheduleProducerService.produce(jobDto);
        }
    }
}
