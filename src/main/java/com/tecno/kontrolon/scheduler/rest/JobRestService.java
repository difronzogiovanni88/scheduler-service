package com.tecno.kontrolon.scheduler.rest;

import com.tecno.kontrolon.scheduler.dao.JobDao;
import com.tecno.kontrolon.scheduler.dto.JobDto;
import com.tecno.kontrolon.scheduler.entity.Job;
import com.tecno.kontrolon.scheduler.service.ScheduleProducerService;
import com.tecno.kontrolon.scheduler.utils.GenericResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@RestController
public class JobRestService {

    @Autowired
    private JobDao jobDao;

    @Autowired
    private ScheduleProducerService scheduleProducerService;

    @RequestMapping(value = "jobs", method = RequestMethod.POST)
    public GenericResponse postJob (@RequestBody @Valid JobDto jobDto, final HttpServletRequest request){
        Job job = jobDao.findByNameAndType(jobDto.getName(), jobDto.getType());
        if (job == null){
            job  = new Job();
            job.setActive(jobDto.isActive());
            job.setName(jobDto.getName());
            job.setScheduleTime(jobDto.getScheduleTime());
            job.setType(jobDto.getType());

        }else{
            job.setActive(jobDto.isActive());
            job.setScheduleTime(jobDto.getScheduleTime());

        }

        String result = scheduleProducerService.produce(jobDto);
        if (result.equals("success"))
            jobDao.save(job);
        return new GenericResponse(result);



    }

    @RequestMapping(value ="jobs/{jobName}", method = RequestMethod.DELETE)
    public GenericResponse deleteJob(@PathVariable String jobName){

        String[] nameType =jobName.split("_", 2);
        Job job = jobDao.findByNameAndType(nameType[1], nameType[0]);
        if(job == null)
           return new GenericResponse("error","error");
        jobDao.delete(job);
       String result= scheduleProducerService.delete(jobName);

        return new GenericResponse(result);

    }

}
