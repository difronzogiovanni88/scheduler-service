package com.tecno.kontrolon.scheduler.dto;

public class JobDto {
    String type;
    String scheduleTime;
    boolean active;
    String name;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getScheduleTime() {
        return scheduleTime;
    }

    public void setScheduleTime(String scheduleTime) {
        this.scheduleTime = scheduleTime;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "JobDto{" +
                "type='" + type + '\'' +
                ", scheduleTime='" + scheduleTime + '\'' +
                ", active=" + active +
                ", name='" + name + '\'' +
                '}';
    }
}
